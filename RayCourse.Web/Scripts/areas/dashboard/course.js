﻿$(document).ready(function () {
    var selected = $("#Type").val();
    var passwordGroup = $("#password-form-group");
    var passwordField = $("#Password");
    if (selected == 1) {
        passwordGroup.removeClass("hidden");
    } else {
        passwordGroup.addClass("hidden");
        passwordField.val("");
    }
    $("#Type").on("change", function () {
        var selected = $(this).val();
        if (selected == 1) {
            passwordGroup.removeClass("hidden");
        } else {
            passwordGroup.addClass("hidden");
            passwordField.val("");
        }
    });
});