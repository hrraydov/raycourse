﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Web.Services
{
    public interface IAuthService
    {
        bool IsLogged();

        string GetCurrentUserId();

        User GetCurrentUser();
    }
}
