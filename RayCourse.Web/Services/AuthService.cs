﻿using RayCourse.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Microsoft.AspNet.Identity;
using RayCourse.Models;

namespace RayCourse.Web.Services
{
    public class AuthService : IAuthService
    {
        private IAppContext db;

        public AuthService(IAppContext db)
        {
            this.db = db;
        }

        public string GetCurrentUserId()
        {
            return Thread.CurrentPrincipal.Identity.GetUserId();
        }

        public User GetCurrentUser()
        {
            string userId = this.GetCurrentUserId();
            return this.db.Find<User>(x => x.Id == userId);
        }

        public bool IsLogged()
        {
            return Thread.CurrentPrincipal.Identity.IsAuthenticated;
        }
    }
}