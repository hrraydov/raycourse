﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RayCourse.Web.Services
{
    public interface ICourseService
    {
        bool IsSetCookie(HttpRequestBase request, Course course);
    }
}
