﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Services
{
    public class CourseService : ICourseService
    {

        public bool IsSetCookie(HttpRequestBase request, Course course)
        {
            var cookie = request.Cookies["course-" + course.Id];
            if (cookie != null && cookie.Value == course.Password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}