﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RayCourse.Web.Startup))]
namespace RayCourse.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
