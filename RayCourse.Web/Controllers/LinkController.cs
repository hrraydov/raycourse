﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Models.Link;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class LinkController : Controller
    {
        private const int pageSize = 6;

        private IAppContext db;

        public LinkController()
        {
            this.db = new AppContext();
        }

        public LinkController(IAppContext db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("link/{id:int}")]
        public ActionResult Show(int id)
        {
            var link = this.db.Find<Link>(x => x.Id == id);
            if (link == null)
            {
                return HttpNotFound();
            }
            var viewModel = new ShowViewModel
            {
                Id = link.Id,
                Title = link.Title,
                Url = link.Url
            };
            return View(viewModel);
        }
    }
}