﻿using Newtonsoft.Json;
using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Models.Test;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class TestController : Controller
    {
        private IAppContext db;
        private ICourseService courseService;

        public TestController()
        {
            this.db = new AppContext();
            this.courseService = new CourseService();
        }

        public TestController(IAppContext db, ICourseService courseService)
        {
            this.db = db;
            this.courseService = courseService;
        }

        [HttpGet]
        [Route("test/{id:int}")]
        public ActionResult Show(int id)
        {
            var test = this.db.Find<Test>(x => x.Id == id, "Course");
            if (test == null)
            {
                return Redirect("/");
            }
            if (test.Course.Type == CourseType.Private && !courseService.IsSetCookie(Request, test.Course))
            {
                return Redirect("/course/" + test.Course.Id + "/activate");
            }
            var viewModel = new ShowViewModel
            {
                Id = test.Id,
                QuestionsCount = test.Questions.Count,
                Name = test.Name,
                Course = test.Course.Id
            };
            return View(viewModel);
        }

        [HttpGet]
        [Route("api/test/{id:int}/question/{number:int}")]
        public ActionResult Question(int id, int number)
        {
            var questions = this.db.All<Question>().Where(x => x.TestId == id).ToList();
            var question = questions[number];
            var viewModel = new QuestionViewModel
            {
                Answers = question.Answers.Select(x => new QuestionViewModel.Answer
                {
                    Content = x.Content,
                    Id = x.Id
                }).ToList(),
                MultipleAnswers = question.MultipleAnswers,
                Title = question.Title
            };
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("api/test/{id:int}/question/{number:int}")]
        public ActionResult QuestionAnswered(int id, int number, [Bind(Include = "Answers")]QuestionAnsweredViewModel viewModel)
        {
            var answers = JsonConvert.DeserializeObject<IEnumerable<int>>(viewModel.Answers);
            var questions = this.db.All<Question>().Where(x => x.TestId == id).ToList();
            var question = questions[number];
            var trueAnswers = JsonConvert.DeserializeObject<ICollection<int>>(question.TrueAnswers);
            viewModel = new QuestionAnsweredViewModel
            {
                Question = new QuestionAnsweredViewModel.QuestionViewModel
                {
                    AnswerDescription = question.AnswerDescription,
                    Answers = question.Answers.Select(x => new QuestionAnsweredViewModel.Answer
                    {
                        Id = x.Id,
                        Content = x.Content,
                        IsAnswered = answers.Contains(x.Id),
                        IsTrue = trueAnswers.Contains(x.Id)
                    }),
                    Title = question.Title,
                    MultipleAnswers = question.MultipleAnswers
                }
            };
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}