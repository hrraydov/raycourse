﻿using RayCourse.Data;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class BaseController : Controller
    {
        protected IAppContext db;

        public BaseController()
            : this(new AppContext())
        {
        }

        public BaseController(IAppContext db)
        {
            this.db = db;
        }
    }
}