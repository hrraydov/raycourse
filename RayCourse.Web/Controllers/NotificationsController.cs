﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Services;
using RayCourse.Web.Models.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    [Authorize]
    public class NotificationsController : Controller
    {
        private IAppContext db;
        private IAuthService auth;

        public NotificationsController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public NotificationsController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var userId = this.auth.GetCurrentUserId();
            var viewModel = new IndexViewModel
            {
                Notifications = this.db.All<Notification>()
                .Where(x => x.RecipientId == userId)
                .OrderByDescending(x => x.Id)
                .Select(x => new IndexViewModel.Notification
                {
                    Parameter1 = x.Parameter1,
                    Parameter2 = x.Parameter2,
                    Type = x.Type,
                    Read = x.Read
                })
            };
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Clear()
        {
            var userId = this.auth.GetCurrentUserId();
            var notifications = this.db.All<Notification>()
                .Where(x => x.RecipientId == userId)
                .ToList();
            foreach (var notification in notifications)
            {
                this.db.Delete<Notification>(notification);
            }
            this.db.SaveChanges();
            return Redirect("/notifications");
        }

        [HttpGet]
        [ActionName("Clear-Read")]
        public ActionResult ClearRead()
        {
            var userId = this.auth.GetCurrentUserId();
            var notifications = this.db.All<Notification>()
                .Where(x => x.RecipientId == userId && x.Read)
                .ToList();
            foreach (var notification in notifications)
            {
                this.db.Delete<Notification>(notification);
            }
            this.db.SaveChanges();
            return Redirect("/notifications");
        }

        [HttpGet]
        [ActionName("Mark-As-Read")]
        public ActionResult MarkAsRead()
        {
            var userId = this.auth.GetCurrentUserId();
            var notifications = this.db.All<Notification>()
                .Where(x => x.RecipientId == userId && !x.Read)
                .ToList();
            foreach (var notification in notifications)
            {
                notification.Read = true;
                this.db.Update<Notification>(notification);
            }
            this.db.SaveChanges();
            return Redirect("/notifications");
        }
    }
}