﻿using RayCourse.Data;
using RayCourse.Web.Services;
using RayCourse.Web.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class HomeController : Controller
    {
        private IAppContext db;
        private IAuthService auth;

        public HomeController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public HomeController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult Menu()
        {
            var viewModel = new MenuViewModel();
            if (this.auth.IsLogged())
            {
                var user = this.auth.GetCurrentUser();
                viewModel.Email = user.Email;
                viewModel.NotificationCount = user.Notifications.Where(x => !x.Read).Count();
                viewModel.Nickname = user.Nickname;
            }
            return PartialView(viewModel);
        }

        [HttpGet]
        [Route("lang/{lang}")]
        public ActionResult Lang(string lang, string returnUrl = "/")
        {
            Response.Cookies["_lang"].Value = lang;
            return Redirect(returnUrl);
        }

    }
}