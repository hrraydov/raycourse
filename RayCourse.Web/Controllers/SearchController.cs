﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Models.Search;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class SearchController : Controller
    {
        private const int pageSize = 5;

        private IAppContext db;

        public SearchController()
        {
            this.db = new AppContext();
        }

        public SearchController(IAppContext db)
        {
            this.db = db;
        }

        [Route("search")]
        public ActionResult Index(string q = "")
        {
            return View(model: q);
        }

        [HttpGet]
        [Route("api/search/courses")]
        public ActionResult Courses(string q = "", int page = 1)
        {
            q = q.ToLower();
            var courses = this.db.All<Course>()
                .Where(x => x.Type != CourseType.Hidden)
                .OrderByDescending(x => x.Name.ToLower().Contains(q))
                .ThenByDescending(x => x.Description.ToLower().Contains(q))
                .ThenByDescending(x => x.FavouriteOf.Count())
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = courses.Select(x => new CoursesViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/search/articles")]
        public ActionResult Articles(string q = "", int page = 1)
        {
            q = q.ToLower();
            var articles = this.db.All<Article>()
                .Include(x => x.Course)
                .OrderByDescending(x => x.Title.ToLower().Contains(q))
                .ThenByDescending(x => x.Content.ToLower().Contains(q))
                .ThenByDescending(x => x.Course.FavouriteOf.Count())
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = articles.Select(x => new ArticlesViewModel
            {
                Id = x.Id,
                Title = x.Title,
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/search/links")]
        public ActionResult Links(string q = "", int page = 1)
        {
            q = q.ToLower();
            var links = this.db.All<Link>()
                .Include(x => x.Course)
                .OrderByDescending(x => x.Title.ToLower().Contains(q))
                .ThenByDescending(x => x.Url.ToLower().Contains(q))
                .ThenByDescending(x => x.Course.FavouriteOf.Count())
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = links.Select(x => new LinksViewModel
            {
                Url = x.Url,
                Title = x.Title,
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/search/tests")]
        public ActionResult Tests(string q = "", int page = 1)
        {
            q = q.ToLower();
            var tests = this.db.All<Test>()
                .Include(x => x.Course)
                .OrderByDescending(x => x.Name.ToLower().Contains(q))
                .ThenByDescending(x => x.Course.FavouriteOf.Count())
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = tests.Select(x => new TestsViewModel
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/search/users")]
        public ActionResult Users(string q = "", int page = 1)
        {
            q = q.ToLower();
            var users = this.db.All<User>()
                .OrderByDescending(x => x.Nickname.ToLower().Contains(q))
                .ThenBy(x => x.FirstName.ToLower().Contains(q))
                .ThenBy(x => x.LastName.ToLower().Contains(q))
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = users.Select(x => new UsersViewModel
            {
                Nickname = x.Nickname,
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}