﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Models.Profile;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class ProfileController : Controller
    {
        private const int pageSize = 10;

        private IAppContext db;

        public ProfileController()
        {
            this.db = new AppContext();
        }

        public ProfileController(IAppContext db)
        {
            this.db = db;
        }

        [ChildActionOnly]
        public ActionResult MostFavourite()
        {
            var users = this.db.All<User>()
                .Include(x => x.OwnedCourses)
                .OrderByDescending(x => x.OwnedCourses.Select(y => y.FavouriteOf.Count()).Sum()).Take(5);
            var viewModel = users.Select(x => x.Nickname).ToList();
            return PartialView(viewModel);
        }

        [HttpGet]
        [Route("user/{nickname}")]
        public ActionResult Show(string nickname)
        {
            var user = this.db.Find<User>(x => x.Nickname == nickname);
            if (user == null)
            {
                return Redirect("/");
            }

            var viewModel = new ShowViewModel
            {
                Id = user.Id,
                Nickname = user.Nickname,
                About = user.About,
                City = user.City,
                Country = user.Country,
                Facebook = user.Facebook,
                FirstName = user.FirstName,
                Google = user.Google,
                LastName = user.LastName,
                LinkedIn = user.LinkedIn,
                Sex = user.Sex,
                Skype = user.Skype,
                Twitter = user.Twitter,
                Website = user.WebSite
            };

            return View(viewModel);
        }

        [HttpGet]
        [Route("api/user/{nickname}/courses/{page:int}")]
        public ActionResult Courses(string nickname, int page)
        {
            var courses = this.db.All<Course>()
                .Include(x => x.Owner)
                .Where(x => x.Type != CourseType.Hidden && x.Owner.Nickname == nickname)
                .OrderByDescending(x => x.FavouriteOf.Count())
                .ThenByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = courses.Select(x => new CoursesViewModel
            {
                Description = x.Description,
                Id = x.Id,
                Name = x.Name
            });

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/user/{nickname}/courses/page-count")]
        public ActionResult CoursesPageCount(string nickname)
        {
            return Json(this.db.All<Course>()
                        .Include(x => x.Owner)
                        .Where(x => x.Type != CourseType.Hidden && x.Owner.Nickname == nickname)
                        .Count() / pageSize + 1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/user/{nickname}/favourite-courses/{page:int}")]
        public ActionResult FavouriteCourses(string nickname, int page)
        {
            var user = this.db.Find<User>(x => x.Nickname == nickname);
            var courses = this.db.All<Course>()
                .Include(x => x.Owner)
                .Where(x => x.Type != CourseType.Hidden && x.FavouriteOf.Select(y => y.Id).Contains(user.Id))
                .OrderByDescending(x => x.FavouriteOf.Count())
                .ThenByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = courses.Select(x => new CoursesViewModel
            {
                Description = x.Description,
                Id = x.Id,
                Name = x.Name
            });

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/user/{nickname}/favourite-courses/page-count")]
        public ActionResult FavouriteCoursesPageCount(string nickname)
        {
            var user = this.db.Find<User>(x => x.Nickname == nickname);
            return Json(this.db.All<Course>()
                        .Include(x => x.Owner)
                        .Where(x => x.Type != CourseType.Hidden && x.FavouriteOf.Select(y => y.Id).Contains(user.Id))
                        .Count() / pageSize + 1, JsonRequestBehavior.AllowGet);
        }
    }
}