﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Services;
using RayCourse.Web.Models;
using RayCourse.Web.Models.Article;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class ArticleController : Controller
    {
        private const int pageSize = 6;

        private IAppContext db;
        private IAuthService auth;
        private ICourseService courseService;

        public ArticleController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
            this.courseService = new CourseService();
        }

        public ArticleController(IAppContext db, IAuthService auth, ICourseService courseService)
        {
            this.db = db;
            this.auth = auth;
            this.courseService = courseService;
        }

        [HttpGet]
        [Route("article/{id:int}")]
        public ActionResult Show(int id)
        {
            var article = this.db.Find<Article>(x => x.Id == id, "Course");
            if (article == null)
            {
                return Redirect("/");
            }
            if (article.Course.Type == CourseType.Private && !courseService.IsSetCookie(Request, article.Course))
            {
                return Redirect("/course/" + article.Course.Id + "/activate");
            }
            var viewModel = new ShowViewModel
            {
                Content = article.Content,
                CreatedOn = article.CreatedOn,
                Id = article.Id,
                Title = article.Title,
                UpdatedOn = article.UpdatedOn
            };
            return View(viewModel);
        }

        [HttpPost]
        [Route("article/{id:int}/add-comment")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult AddComment(string content, int id)
        {
            var article = this.db.Find<Article>(x => x.Id == id, "Course");
            if (article == null)
            {
                return Redirect("/");
            }
            if (article.Course.Type == CourseType.Private && !courseService.IsSetCookie(Request, article.Course))
            {
                return Redirect("/course/" + article.Course.Id + "/activate");
            }
            article.Comments.Add(new Comment
            {
                ArticleId = article.Id,
                Content = content,
                CreatedOn = DateTime.Now,
                UserId = this.auth.GetCurrentUserId()
            });
            this.db.Add<Notification>(new Notification
            {
                Parameter1 = article.Title,
                Type = NotificationType.ArticleCommented,
                RecipientId = article.Course.OwnerId
            });
            this.db.Update<Article>(article);
            this.db.SaveChanges();
            return Redirect("/article/" + id);
        }

        [HttpGet]
        [Route("api/article/{id:int}/comments/{page:int}")]
        public ActionResult Comments(int id, int page)
        {
            var comments = this.db.All<Comment>()
                .Where(x => x.ArticleId == id)
                .Include(x => x.User)
                .OrderByDescending(x => x.CreatedOn)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);
            var viewModel = comments.ToList().Select(x => new CommentViewModel
            {
                Content = x.Content,
                CreatedOn = x.CreatedOn.ToString("HH:mm:ss d.MM.yyyy"),
                Id = x.Id,
                User = x.User.Nickname
            });
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/article/{id:int}/comments/page-count")]
        public ActionResult CommentsPageCount(int id)
        {
            return Json(this.db.All<Comment>().Where(x => x.ArticleId == id).Count() / pageSize + 1, JsonRequestBehavior.AllowGet);
        }
    }
}