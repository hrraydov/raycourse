﻿using RayCourse.Data;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class CommentController : Controller
    {
        private IAppContext db;
        private IAuthService auth;

        public CommentController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public CommentController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }
    }
}