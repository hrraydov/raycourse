﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Services;
using RayCourse.Web.Models.Courses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Controllers
{
    public class CoursesController : Controller
    {
        private IAppContext db;
        private IAuthService auth;
        private ICourseService courseService;

        private const int pageSize = 6;

        public CoursesController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(db);
            this.courseService = new CourseService();
        }

        public CoursesController(IAppContext db, IAuthService auth, ICourseService courseService)
        {
            this.db = db;
            this.auth = auth;
            this.courseService = courseService;
        }

        [ChildActionOnly]
        public ActionResult MostFavourite()
        {
            var courses = this.db.All<Course>().Where(x => x.Type != CourseType.Hidden).Include(x => x.Owner).OrderByDescending(x => x.FavouriteOf.Count).ThenBy(x => x.Name).Take(5);
            var viewModel = courses.Select(x => new MostFavouriteViewModel
            {
                Name = x.Name,
                Owner = x.Owner.Nickname,
                Id = x.Id
            });
            return PartialView(viewModel);
        }


        [HttpGet]
        [Route("course/{id:int}")]
        public ActionResult Show(int id)
        {
            var course = this.db.Find<Course>(x => x.Id == id, "Owner");
            if (course == null)
            {
                return Redirect("/");
            }
            if (course.Type == CourseType.Private && !courseService.IsSetCookie(Request, course))
            {
                return Redirect("/course/" + course.Id + "/activate");
            }
            var viewModel = new ShowViewModel
            {
                Owner = course.Owner.Nickname,
                OwnerId = course.Owner.Id,
                Id = course.Id,
                Name = course.Name,
                IsFavourite = false,
                Description = course.Description,
                IsOwner = course.Owner.Id == this.auth.GetCurrentUserId(),
            };
            if (this.auth.IsLogged() && this.auth.GetCurrentUser().FavouriteCourses.Contains(course))
            {
                viewModel.IsFavourite = true;
            }
            return View(viewModel);
        }

        [HttpGet]
        [Route("course/{id:int}/activate")]
        public ActionResult Activate(int id)
        {
            var course = this.db.Find<Course>(x => x.Id == id);
            if (course == null)
            {
                return Redirect("/");
            }
            var viewModel = new ActivateViewModel
            {
                CourseId = course.Id
            };
            return View(viewModel);
        }

        [HttpPost]
        [Route("course/{id:int}/activate")]
        [ValidateAntiForgeryToken]
        public ActionResult Activate(int id, [Bind(Include = "CourseId, Password")]ActivateViewModel viewModel)
        {
            var course = this.db.Find<Course>(x => x.Id == id);
            if (course == null)
            {
                return Redirect("/");
            }
            if (course.Password != viewModel.Password)
            {
                ModelState.AddModelError("", RayCourse.Resources.Validation.WrongPassword);
                return View(viewModel);
            }

            HttpCookie cookie = new HttpCookie("course-" + course.Id);

            cookie.Value = course.Password;
            cookie.Expires = DateTime.Now.AddDays(1);

            Response.Cookies.Add(cookie);

            return Redirect("/course/" + course.Id);
        }

        [HttpGet]
        [Route("course/{id:int}/add-to-favourites")]
        [Authorize]
        public ActionResult AddToFavourites(int id)
        {
            var course = this.db.Find<Course>(x => x.Id == id);
            if (course == null)
            {
                return Redirect("/");
            }

            var user = this.auth.GetCurrentUser();

            if (user.Id == course.OwnerId)
            {
                return Redirect("/course/" + course.Id);
            }

            user.FavouriteCourses.Add(course);

            this.db.Add<Notification>(new Notification
            {
                Parameter1 = user.Nickname,
                Parameter2 = course.Name,
                Type = NotificationType.AddedToFavourites,
                RecipientId = course.OwnerId
            });
            this.db.Update<User>(user);
            this.db.SaveChanges();

            return Redirect("/course/" + course.Id);
        }

        [HttpGet]
        [Route("course/{id:int}/remove-from-favourites")]
        [Authorize]
        public ActionResult RemoveFromFavourites(int id)
        {
            var course = this.db.Find<Course>(x => x.Id == id);
            if (course == null)
            {
                return Redirect("/");
            }

            var user = this.auth.GetCurrentUser();

            if (user.Id == course.OwnerId)
            {
                return Redirect("/course/" + course.Id);
            }

            if (!user.FavouriteCourses.Contains(course))
            {
                return Redirect("/course/" + course.Id);
            }

            user.FavouriteCourses.Remove(course);
            this.db.Update<User>(user);
            this.db.SaveChanges();

            return Redirect("/course/" + course.Id);
        }

        [HttpGet]
        [Route("api/course/{id:int}/articles/{page:int}")]
        public ActionResult Articles(int id, int page)
        {
            var articles = this.db.All<Article>()
                .Where(x => x.CourseId == id)
                .OrderByDescending(x => x.CreatedOn)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = articles.Select(x => new ArticlesViewModel
            {
                Id = x.Id,
                Title = x.Title
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/course/{id:int}/articles/page-count")]
        public ActionResult ArticlesPageCount(int id)
        {
            return Json(this.db.All<Article>().Where(x => x.CourseId == id).Count() / pageSize + 1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/course/{id:int}/links/{page:int}")]
        public ActionResult Links(int id, int page)
        {
            var links = this.db.All<Link>()
                .Where(x => x.CourseId == id)
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = links.Select(x => new LinksViewModel
            {
                Id = x.Id,
                Title = x.Title,
                Url = x.Url
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/course/{id:int}/links/page-count")]
        public ActionResult LinksPageCount(int id)
        {
            return Json(this.db.All<Link>().Where(x => x.CourseId == id).Count() / pageSize + 1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/course/{id:int}/tests/{page:int}")]
        public ActionResult Tests(int id, int page)
        {
            var tests = this.db.All<Test>()
                .Where(x => x.CourseId == id)
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var viewModel = tests.Select(x => new TestsViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("api/course/{id:int}/tests/page-count")]
        public ActionResult TestsPageCount(int id)
        {
            return Json(this.db.All<Test>().Where(x => x.CourseId == id).Count() / pageSize + 1, JsonRequestBehavior.AllowGet);
        }


    }
}
