﻿using Newtonsoft.Json;
using RayCourse.Data;
using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Filters
{
    public class LogActionFilter : ActionFilterAttribute, IActionFilter
    {
        private Log log = new Log();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.User.IsInRole("Admin"))
            {
                var url = filterContext.HttpContext.Request.RawUrl.ToLower();
                string rawArea = null;
                if (url.StartsWith("/dashboard"))
                {
                    rawArea = "dashboard";
                }
                else if (url.StartsWith("/admin"))
                {
                    rawArea = "admin";
                }
                var area = rawArea != null ? rawArea.ToString().ToLower() : "";
                Request request = new Request
                {
                    Action = filterContext.ActionDescriptor.ActionName.ToLower(),
                    Area = area,
                    Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                    IsApiAction = url.StartsWith("/api"),
                    IsChildAction = filterContext.IsChildAction,
                    Method = filterContext.HttpContext.Request.HttpMethod.ToLower(),
                    Params = JsonConvert.SerializeObject(filterContext.ActionParameters),
                    Uri = url,
                };
                log.Request = request;
                log.DateTime = DateTime.Now;
                log.Email = filterContext.HttpContext.User.Identity.Name;
                log.IP = filterContext.HttpContext.Request.UserHostAddress;
                using (AppContext db = new AppContext())
                {
                    db.Add<Log>(log);
                    db.SaveChanges();
                }
            }
            base.OnActionExecuting(filterContext);
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (!filterContext.HttpContext.User.IsInRole("Admin"))
            {
            }
            base.OnResultExecuted(filterContext);
        }
    }
}