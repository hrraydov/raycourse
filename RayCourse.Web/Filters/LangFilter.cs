﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Filters
{
    public class LangFilter : ActionFilterAttribute, IActionFilter
    {
        HashSet<string> allowedLanguages = new HashSet<string>() { 
            "en",
            "bg"
        };
        string defaultLanguage = "en";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cultureName = "";

            if (filterContext.HttpContext.Request.Cookies["_lang"] == null)
            {
                HttpCookie cookie = new HttpCookie("_lang");
                if (cookie.Value == null)
                {
                    cookie.Expires = DateTime.Now.AddDays(1);
                    cookie.Value = "en";
                }
                filterContext.HttpContext.Request.Cookies.Add(cookie);
                filterContext.HttpContext.Response.Cookies.Add(cookie);
            }
            cultureName = filterContext.HttpContext.Request.Cookies["_lang"].Value.ToString();

            if (!allowedLanguages.Contains(cultureName))
            {
                cultureName = defaultLanguage;
            }

            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureName);
            base.OnActionExecuting(filterContext);
        }
    }
}