﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayCourse.Web.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Email { get; set; }

        [Required(
            ErrorMessageResourceName = "NicknameRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Nickname",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Nickname { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.Fields)
            )]
        [EmailAddress(
            ErrorMessage=null,
            ErrorMessageResourceName = "EmailValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string Email { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [DataType(DataType.Password)]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Password { get; set; }

        [Display(
            Name = "RememberMe",
            ResourceType = typeof(Resources.Fields)
            )]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.Fields)
            )]
        [EmailAddress(
            ErrorMessage=null,
            ErrorMessageResourceName = "EmailValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string Email { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [RegularExpression(
            "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$",
            ErrorMessageResourceName="PasswordValid",
            ErrorMessageResourceType=typeof(Resources.Validation)
            )]
        [DataType(DataType.Password)]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(
            Name = "PasswordConfirm",
            ResourceType=typeof(Resources.Fields)
            )]
        [Compare(
            "Password",
            ErrorMessageResourceName="PasswordsMatch",
            ErrorMessageResourceType=typeof(Resources.Validation)
            )]
        public string ConfirmPassword { get; set; }

        [Required(
            ErrorMessageResourceName = "NicknameRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Nickname",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Nickname { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.Fields)
            )]
        [EmailAddress(
            ErrorMessage=null,
            ErrorMessageResourceName = "EmailValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string Email { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [RegularExpression(
            "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$",
            ErrorMessageResourceName = "PasswordValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [DataType(DataType.Password)]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(
            Name = "PasswordConfirm",
            ResourceType = typeof(Resources.Fields)
            )]
        [Compare(
            "Password",
            ErrorMessageResourceName = "PasswordsMatch",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.Fields)
            )]
        [EmailAddress(
            ErrorMessage = null,
            ErrorMessageResourceName = "EmailValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string Email { get; set; }
    }
}
