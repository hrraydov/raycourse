﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Courses
{
    public class ActivateViewModel
    {
        [Required]
        public int CourseId { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.Fields)
            )]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}