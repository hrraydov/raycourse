﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Courses
{
    public class LinksViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }
    }
}