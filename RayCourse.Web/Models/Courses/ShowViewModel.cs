﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Courses
{
    public class ShowViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public string OwnerId { get; set; }

        public bool IsFavourite { get; set; }

        public bool IsOwner { get; set; }
    }
}