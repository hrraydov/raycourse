﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Home
{
    public class MenuViewModel
    {
        public string Nickname { get; set; }

        public int NotificationCount { get; set; }

        public string Email { get; set; }
    }
}