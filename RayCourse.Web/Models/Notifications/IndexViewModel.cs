﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Notifications
{
    public class IndexViewModel
    {
        public IEnumerable<Notification> Notifications { get; set; }

        public class Notification
        {
            public bool Read { get; set; }

            public string Content { get; set; }

            public string Parameter1 { get; set; }

            public string Parameter2 { get; set; }

            public NotificationType Type { get; set; }
        }
    }
}