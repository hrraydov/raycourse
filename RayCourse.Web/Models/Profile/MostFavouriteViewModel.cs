﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Profile
{
    public class MostFavouriteViewModel
    {
        public string Nickname { get; set; }
    }
}