﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Profile
{
    public class ShowViewModel
    {
        public string Id { get; set; }

        public string Nickname { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Sex Sex { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string About { get; set; }

        public string Website { get; set; }

        public string Skype { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Google { get; set; }

        public string LinkedIn { get; set; }
    }
}