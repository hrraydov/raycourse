﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Search
{
    public class CoursesViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string UrlName { get; set; }
    }
}