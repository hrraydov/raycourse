﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Search
{
    public class LinksViewModel
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}