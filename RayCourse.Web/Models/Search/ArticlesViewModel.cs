﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Search
{
    public class ArticlesViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}