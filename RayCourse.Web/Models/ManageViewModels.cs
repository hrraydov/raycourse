﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace RayCourse.Web.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [RegularExpression(
            "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$",
            ErrorMessageResourceName = "PasswordValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [DataType(DataType.Password)]
        [Display(
            Name = "NewPassword",
            ResourceType = typeof(Resources.Fields)
            )]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(
            Name = "PasswordConfirm",
            ResourceType = typeof(Resources.Fields)
            )]
        [Compare(
            "NewPassword",
            ErrorMessageResourceName = "PasswordsMatch",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [DataType(DataType.Password)]
        [Display(
            Name = "OldPassword",
            ResourceType = typeof(Resources.Fields)
            )]
        public string OldPassword { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [RegularExpression(
            "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$",
            ErrorMessageResourceName = "PasswordValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [DataType(DataType.Password)]
        [Display(
            Name = "NewPassword",
            ResourceType = typeof(Resources.Fields)
            )]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(
            Name = "PasswordConfirm",
            ResourceType = typeof(Resources.Fields)
            )]
        [Compare(
            "NewPassword",
            ErrorMessageResourceName = "PasswordsMatch",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}