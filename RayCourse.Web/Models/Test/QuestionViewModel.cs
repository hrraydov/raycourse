﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Test
{
    public class QuestionViewModel
    {
        public string Title { get; set; }

        public bool MultipleAnswers { get; set; }

        public IEnumerable<Answer> Answers { get; set; }

        public class Answer
        {
            public int Id { get; set; }

            public string Content { get; set; }
        }
    }
}