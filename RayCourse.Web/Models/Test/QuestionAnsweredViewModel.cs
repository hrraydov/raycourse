﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Test
{
    public class QuestionAnsweredViewModel
    {
        public string Answers { get; set; }

        public QuestionViewModel Question { get; set; }

        public class QuestionViewModel
        {
            public string Title { get; set; }

            public string AnswerDescription { get; set; }

            public IEnumerable<Answer> Answers { get; set; }

            public bool MultipleAnswers { get; set; }
        }

        public class Answer
        {
            public int Id { get; set; }

            public bool IsTrue { get; set; }

            public string Content { get; set; }

            public bool IsAnswered { get; set; }
        }


    }
}