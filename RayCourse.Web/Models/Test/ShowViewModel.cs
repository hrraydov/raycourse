﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Test
{
    public class ShowViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Course { get; set; }

        public int QuestionsCount { get; set; }
    }
}