﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Comment
{
    public class AddCommentViewModel
    {
        [Required(
            ErrorMessageResourceName = "ContentRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [DataType(DataType.MultilineText)]
        [Display(
            Name = "Content",
            ResourceType = typeof(Resources.Fields)
        )]
        public string Content { get; set; }
    }
}