﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Models.Link
{
    public class ShowViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }
    }
}