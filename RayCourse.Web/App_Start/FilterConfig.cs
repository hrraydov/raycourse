﻿using RayCourse.Web.Filters;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogActionFilter());
            filters.Add(new LangFilter());
        }
    }
}
