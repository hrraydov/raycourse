﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Areas.Dashboard.Models.Articles;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Controllers
{
    [RouteArea("Dashboard")]
    [Authorize]
    public class ArticlesController : Controller
    {
        private const int pageSize = 10;

        private IAppContext db;
        private IAuthService auth;

        public ArticlesController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public ArticlesController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }

        [HttpGet]
        public ActionResult Index(int page = 1, string sortBy = "")
        {
            var userId = this.auth.GetCurrentUserId();
            var articles = this.db.All<Article>().Include("Course").Where(x => x.Course.OwnerId == userId);
            switch (sortBy.ToLower())
            {
                case "course":
                    articles = articles.OrderBy(x => x.Course.Name);
                    break;
                case "title":
                    articles = articles.OrderBy(x => x.Title);
                    break;
                default:
                    articles = articles.OrderByDescending(x => x.CreatedOn);
                    break;
            }
            articles = articles.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new IndexViewModel
            {
                Articles = articles.Select(x => new IndexViewModel.Article
                {
                    Course = x.Course.Name,
                    CourseId = x.Course.Id,
                    CreatedOn = x.CreatedOn,
                    Id = x.Id,
                    Title = x.Title
                }).ToList(),
                Page = page,
                PageCount = this.db.Count<Article>(x => x.Course.OwnerId == userId) / 10 + 1,
                SortBy = sortBy
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var article = this.db.Find<Article>(x => x.Id == id, "Course");
            if (article == null)
            {
                return HttpNotFound();
            }
            if (article.Course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new DetailsViewModel
            {
                Content = article.Content,
                CourseId = article.Course.Id,
                Id = article.Id,
                Title = article.Title,
                CreatedOn = article.CreatedOn,
                UpdatedOn = article.UpdatedOn
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var userId = this.auth.GetCurrentUserId();
            var viewModel = new CreateViewModel
            {
                Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title, Content, Course")]CreateViewModel viewModel)
        {
            var userId = this.auth.GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                viewModel.Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                return View(viewModel);
            }

            var article = new Article
            {
                Content = viewModel.Content,
                CourseId = viewModel.Course,
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now,
                Title = viewModel.Title
            };

            foreach (var user in this.db.Find<Course>(x => x.Id == viewModel.Course).FavouriteOf)
            {
                this.db.Add<Notification>(new Notification
                {
                    Parameter1 = this.auth.GetCurrentUser().Nickname,
                    Parameter2 = article.Title,
                    Type = NotificationType.ArticleAdded,
                    RecipientId = user.Id
                });
            }
            this.db.Add<Article>(article);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var article = this.db.Find<Article>(x => x.Id == id, "Course");
            if (article == null)
            {
                return HttpNotFound();
            }
            if (article.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new EditViewModel
            {
                Content = article.Content,
                Course = article.Course.Id,
                Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                Id = article.Id,
                Title = article.Title
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Title, Content, Course")]EditViewModel viewModel)
        {
            var userId = this.auth.GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                viewModel.Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                return View(viewModel);
            }
            var article = this.db.Find<Article>(x => x.Id == viewModel.Id, "Course");
            if (article == null)
            {
                return HttpNotFound();
            }
            if (article.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            article.Content = viewModel.Content;
            article.CourseId = viewModel.Course;
            article.Title = viewModel.Title;
            article.UpdatedOn = DateTime.Now;

            this.db.Update<Article>(article);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var article = this.db.Find<Article>(x => x.Id == id, "Course");
            if (article == null)
            {
                return HttpNotFound();
            }
            if (article.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            this.db.Delete<Article>(article);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        private ActionResult redirectToIndex()
        {
            return Redirect("/dashboard/articles");
        }
    }
}
