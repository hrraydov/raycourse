﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Areas.Dashboard.Models.Courses;
using RayCourse.Web.Services;
using RayCourse.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Controllers
{
    [RouteArea("Dashboard")]
    [Authorize]
    public class CoursesController : Controller
    {
        private const int pageSize = 10;

        private IAppContext db;
        private IAuthService auth;

        public CoursesController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public CoursesController(IAppContext db, IAuthService auth)
        {
            this.db = db ?? new AppContext();
            this.auth = auth ?? new AuthService(this.db);
        }

        [HttpGet]
        public ActionResult Index(int page = 1, string sortBy = "name")
        {
            string userId = this.auth.GetCurrentUserId();
            IQueryable<Course> courses = this.db.All<Course>().Where(x => x.OwnerId == userId);

            switch (sortBy.ToLower())
            {
                case "type": courses = courses.OrderBy(x => x.Type); break;
                default: courses = courses.OrderBy(x => x.Name); break;
            }

            courses = courses.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new IndexViewModel
            {
                Courses = courses.Select(x => new IndexViewModel.Course
                {
                    Id = x.Id,
                    Name = x.Name,
                    Type = x.Type,
                   
                }).ToList(),
                Page = page,
                PageCount = this.db.Count<Course>(x => x.OwnerId == userId) / pageSize + 1,
                SortBy = sortBy
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, Description, Type, Password")]CreateViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var course = new Course
            {
                Description = viewModel.Description,
                Name = viewModel.Name,
                OwnerId = this.auth.GetCurrentUserId(),
                Password = viewModel.Password,
                Type = viewModel.Type
            };
            this.db.Add<Course>(course);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var course = this.db.Find<Course>(x => x.Id == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            if (course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new EditViewModel
            {
                Description = course.Description,
                Id = course.Id,
                Name = course.Name,
                Password = course.Password,
                Type = course.Type
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name,  Description, Type, Password")]EditViewModel viewModel)
        {
            var course = this.db.Find<Course>(x => x.Id == viewModel.Id);
            if (course == null)
            {
                return HttpNotFound();
            }
            if (course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }

            course.Description = viewModel.Description;
            course.Name = viewModel.Name;
            course.Password = viewModel.Password;
            course.Type = viewModel.Type;

            this.db.Update<Course>(course);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var course = this.db.Find<Course>(x => x.Id == id, "Articles, Links, Tests");
            if (course == null)
            {
                return HttpNotFound();
            }
            if (course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new DetailsViewModel
            {
                Description = course.Description,
                Id = course.Id,
                Name = course.Name,
                Password = course.Password,
                Type = course.Type,
                Articles = course.Articles.OrderByDescending(x => x.Id).Take(8).Select(x => new DetailsViewModel.Article
                {
                    Id = x.Id,
                    Title = x.Title
                }),
                Links = course.Links.OrderByDescending(x => x.Id).Take(8).Select(x => new DetailsViewModel.Link
                {
                    Id = x.Id,
                    Title = x.Title
                }),
                Tests = course.Tests.OrderByDescending(x => x.Id).Take(8).Select(x => new DetailsViewModel.Test
                {
                    Id = x.Id,
                    Name = x.Name
                })
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var course = this.db.Find<Course>(x => x.Id == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            if (course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }
            this.db.Delete<Course>(course);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [ChildActionOnly]
        public ActionResult BriefDetails(int id)
        {
            var course = this.db.Find<Course>(x => x.Id == id);
            var viewModel = new BriefDetailsViewModel
            {
                Description = course.Description,
                Id = course.Id,
                Name = course.Name
            };
            return PartialView(viewModel);
        }

        private ActionResult redirectToIndex()
        {
            return Redirect("/dashboard/courses");
        }
    }
}