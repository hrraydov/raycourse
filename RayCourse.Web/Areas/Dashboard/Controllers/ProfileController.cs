﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Areas.Dashboard.Models.Profile;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Controllers
{
    [Authorize]
    [RouteArea("Dashboard")]
    public class ProfileController : Controller
    {
        private IAppContext db;
        private IAuthService auth;

        public ProfileController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public ProfileController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }

        [HttpGet]
        [Route("profile")]
        public ActionResult Edit()
        {
            var user = this.auth.GetCurrentUser();

            var viewModel = new EditViewModel
            {
                About = user.About,
                City = user.City,
                Country = user.Country,
                Facebook = user.Facebook,
                FirstName = user.FirstName,
                Google = user.Google,
                LastName = user.LastName,
                LinkedIn = user.LinkedIn,
                Sex = user.Sex,
                Twitter = user.Twitter,
                WebSite = user.WebSite,
            };
            return View(viewModel);
        }

        [HttpPost]
        [Route("profile")]
        public ActionResult Edit(EditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = this.auth.GetCurrentUser();

            user.About = viewModel.About;
            user.City = viewModel.City;
            user.Country = viewModel.Country;
            user.Facebook = viewModel.Facebook;
            user.FirstName = viewModel.FirstName;
            user.Google = viewModel.Google;
            user.LastName = viewModel.LastName;
            user.LinkedIn = viewModel.LinkedIn;
            user.Sex = viewModel.Sex;
            user.Twitter = viewModel.Twitter;
            user.WebSite = viewModel.WebSite;

            this.db.Update<User>(user);
            this.db.SaveChanges();
            return Redirect("/dashboard/profile");
        }
    }
}