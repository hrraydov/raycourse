﻿using Newtonsoft.Json;
using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Areas.Dashboard.Models.Tests;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Controllers
{
    [RouteArea("Dashboard")]
    [Authorize]
    public class TestsController : Controller
    {
        private const int pageSize = 10;

        private IAppContext db;
        private IAuthService auth;

        public TestsController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public TestsController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }

        [HttpGet]
        public ActionResult Index(int page = 1, string sortBy = "")
        {
            var userId = this.auth.GetCurrentUserId();
            var tests = this.db.All<Test>().Include("Course").Where(x => x.Course.OwnerId == userId);
            switch (sortBy.ToLower())
            {
                case "name":
                    tests = tests.OrderBy(x => x.Name);
                    break;
                case "course":
                    tests = tests.OrderBy(x => x.Course.Name);
                    break;
                default:
                    tests = tests.OrderByDescending(x => x.Id);
                    break;
            }
            tests = tests.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new IndexViewModel
            {
                Tests = tests.Select(x => new IndexViewModel.Test
                {
                    Course = x.Course.Name,
                    CourseId = x.Course.Id,
                    Name = x.Name,
                    Id = x.Id,
                }).ToList(),
                Page = page,
                PageCount = this.db.Count<Test>(x => x.Course.OwnerId == userId) / 10 + 1,
                SortBy = sortBy
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var test = this.db.Find<Test>(x => x.Id == id, "Course, Questions, Questions.Answers");
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new DetailsViewModel
            {
                Id = test.Id,
                Name = test.Name,
                Questions = new HashSet<DetailsViewModel.Question>()
            };

            foreach (var question in test.Questions)
            {
                ICollection<int> trueAnswerIds = JsonConvert.DeserializeObject<ICollection<int>>(question.TrueAnswers);
                viewModel.Questions.Add(new DetailsViewModel.Question
                {
                    AnswerDescription = question.AnswerDescription,
                    Id = question.Id,
                    MultipleAnswers = question.MultipleAnswers,
                    Title = question.Title,
                    Answers = question.Answers.Select(x => new DetailsViewModel.Answer
                    {
                        Content = x.Content,
                        Id = x.Id,
                        IsTrue = trueAnswerIds.Contains(x.Id)
                    }).ToList(),
                });
            }

            return View(viewModel);

        }

        [HttpGet]
        public ActionResult Create()
        {
            var userId = this.auth.GetCurrentUserId();
            var viewModel = new CreateViewModel
            {
                Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, Course")]CreateViewModel viewModel)
        {
            var userId = this.auth.GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                viewModel.Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                return View(viewModel);
            }

            var test = new Test
            {
                CourseId = viewModel.Course,
                Name = viewModel.Name
            };

            foreach (var user in this.db.Find<Course>(x => x.Id == viewModel.Course).FavouriteOf)
            {
                this.db.Add<Notification>(new Notification
                {
                    Parameter1 = this.auth.GetCurrentUser().Nickname,
                    Parameter2 = test.Name,
                    Type = NotificationType.TestAdded,
                    RecipientId = user.Id
                });
            }
            this.db.Add<Test>(test);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var test = this.db.Find<Test>(x => x.Id == id, "Course");
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new EditViewModel
            {
                Course = test.Course.Id,
                Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                Id = test.Id,
                Name = test.Name
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, Course")]EditViewModel viewModel)
        {
            var userId = this.auth.GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                viewModel.Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                return View(viewModel);
            }
            var test = this.db.Find<Test>(x => x.Id == viewModel.Id, "Course");
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            test.CourseId = viewModel.Course;
            test.Name = viewModel.Name;

            this.db.Update<Test>(test);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var test = this.db.Find<Test>(x => x.Id == id, "Course");
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            this.db.Delete<Test>(test);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        [Route("tests/{id:int}/questions/add")]
        public ActionResult AddQuestion(int id)
        {
            return View(new AddQuestionViewModel
            {
                TestId = id
            });
        }

        [HttpPost]
        [Route("tests/{id:int}/questions/add")]
        public ActionResult AddQuestion(int id, [Bind(Include = "Title, MultipleAnswers, AnswerDescription, Answers")]AddQuestionViewModel viewModel)
        {
            foreach (var answer in viewModel.Answers)
            {
                if (String.IsNullOrWhiteSpace(answer.Content))
                {
                    viewModel.Answers.Remove(answer);
                }
            }
            var userId = this.auth.GetCurrentUserId();
            var test = this.db.Find<Test>(x => x.Id == id, "Course");
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }
            var question = new Question
            {
                AnswerDescription = viewModel.AnswerDescription,
                MultipleAnswers = viewModel.MultipleAnswers,
                TestId = id,
                Title = viewModel.Title,
                TrueAnswers = "temporary"
            };
            foreach (var answer in viewModel.Answers)
            {
                question.Answers.Add(new Answer
                {
                    Content = answer.Content,
                });
            }
            this.db.Add<Question>(question);
            this.db.SaveChanges();

            question = this.db.Find<Question>(x => x.Title == viewModel.Title && x.TestId == test.Id, "Answers");

            ICollection<int> trueAnswerIds = new HashSet<int>();
            foreach (var dbAnswer in question.Answers)
            {
                var answer = viewModel.Answers.FirstOrDefault(x => x.Content == dbAnswer.Content);
                if (answer != null && answer.IsTrue)
                {
                    trueAnswerIds.Add(dbAnswer.Id);
                    if (question.MultipleAnswers == false)
                    {
                        break;
                    }
                    viewModel.Answers.Remove(answer);
                }
            }
            question.TrueAnswers = JsonConvert.SerializeObject(trueAnswerIds);
            this.db.Update<Question>(question);
            this.db.SaveChanges();

            return Redirect("/dashboard/tests/details/" + id);
        }

        [HttpGet]
        [Route("questions/edit/{id:int}")]
        public ActionResult EditQuestion(int id)
        {
            var question = this.db.Find<Question>(x => x.Id == id, "Test.Course");
            if (question == null)
            {
                return Redirect("/dashboard/tests");
            }
            if (question.Test.Course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }
            var trueAnswers = JsonConvert.DeserializeObject<ICollection<int>>(question.TrueAnswers);
            return View(new EditQuestionViewModel
            {
                TestId = question.TestId,
                Id = id,
                AnswerDescription = question.AnswerDescription,
                Answers = question.Answers.Select(x => new EditQuestionViewModel.Answer
                {
                    Content = x.Content,
                    IsTrue = trueAnswers.Contains(x.Id)
                }).ToList(),
                MultipleAnswers = question.MultipleAnswers,
                Title = question.Title
            });
        }

        [HttpPost]
        [Route("questions/edit/{id:int}")]
        public ActionResult EditQuestion(int id, [Bind(Include = "Id, Title, MultipleAnswers, AnswerDescription, Answers, TestId")]EditQuestionViewModel viewModel)
        {
            var question = this.db.Find<Question>(x => x.Id == id, "Test.Course");
            if (question == null)
            {
                return Redirect("/dashboard/tests");
            }
            if (question.Test.Course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }
            var answersToDelete = new HashSet<int>();
            foreach (var answer in question.Answers)
            {
                answersToDelete.Add(answer.Id);
            }
            foreach (var answer in answersToDelete)
            {
                this.db.DeleteBy<Answer>(x => x.Id == answer);
            }
            this.db.SaveChanges();
            question = this.db.Find<Question>(x => x.Id == id, "Test.Course");
            foreach (var answer in viewModel.Answers)
            {
                if (String.IsNullOrWhiteSpace(answer.Content))
                {
                    viewModel.Answers.Remove(answer);
                }
            }
            var userId = this.auth.GetCurrentUserId();
            var test = this.db.Find<Test>(x => x.Id == viewModel.TestId, "Course");
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }
            question.AnswerDescription = viewModel.AnswerDescription;
            question.MultipleAnswers = viewModel.MultipleAnswers;
            question.Title = viewModel.Title;
            question.TrueAnswers = "temporary";
            foreach (var answer in viewModel.Answers)
            {
                question.Answers.Add(new Answer
                {
                    Content = answer.Content,
                });
            }
            this.db.Update<Question>(question);
            this.db.SaveChanges();

            question = this.db.Find<Question>(x => x.Title == viewModel.Title && x.TestId == test.Id, "Answers");

            ICollection<int> trueAnswerIds = new HashSet<int>();
            foreach (var dbAnswer in question.Answers)
            {
                var answer = viewModel.Answers.FirstOrDefault(x => x.Content == dbAnswer.Content);
                if (answer != null && answer.IsTrue)
                {
                    trueAnswerIds.Add(dbAnswer.Id);
                    if (question.MultipleAnswers == false)
                    {
                        break;
                    }
                    viewModel.Answers.Remove(answer);
                }
            }
            question.TrueAnswers = JsonConvert.SerializeObject(trueAnswerIds);
            this.db.Update<Question>(question);
            this.db.SaveChanges();

            return Redirect("/dashboard/tests/details/" + viewModel.TestId);
        }

        [HttpGet]
        [Route("questions/delete/{id:int}")]
        public ActionResult Delete(int id)
        {
            var userId = this.auth.GetCurrentUserId();
            var question = this.db.Find<Question>(x => x.Id == id, "Test.Course");
            if (question == null)
            {
                return HttpNotFound();
            }
            if (question.Test.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }
            var testId = question.TestId;

            this.db.Delete<Question>(question);
            this.db.SaveChanges();

            return Redirect("/dashboard/tests/details/" + testId);
        }

        private ActionResult redirectToIndex()
        {
            return Redirect("/dashboard/tests");
        }
    }
}