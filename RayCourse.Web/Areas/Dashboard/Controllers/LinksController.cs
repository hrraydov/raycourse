﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Areas.Dashboard.Models.Links;
using RayCourse.Web.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Controllers
{
    [Authorize]
    [RouteArea("Dashboard")]
    public class LinksController : Controller
    {
        private const int pageSize = 10;

        private IAppContext db;
        private IAuthService auth;

        public LinksController()
        {
            this.db = new AppContext();
            this.auth = new AuthService(this.db);
        }

        public LinksController(IAppContext db, IAuthService auth)
        {
            this.db = db;
            this.auth = auth;
        }

        [HttpGet]
        public ActionResult Index(int page = 1, string sortBy = "")
        {
            var userId = this.auth.GetCurrentUserId();
            var links = this.db.All<Link>().Include("Course").Where(x => x.Course.OwnerId == userId);
            switch (sortBy.ToLower())
            {
                case "course":
                    links = links.OrderBy(x => x.Course.Name);
                    break;
                case "url":
                    links = links.OrderBy(x => x.Url);
                    break;
                default:
                    links = links.OrderByDescending(x => x.Title);
                    break;
            }
            links = links.Skip((page - 1) * pageSize).Take(pageSize);

            var viewModel = new IndexViewModel
            {
                Links = links.Select(x => new IndexViewModel.Link
                {
                    Course = x.Course.Name,
                    CourseId = x.Course.Id,
                    Id = x.Id,
                    Title = x.Title,
                    Url = x.Url
                }).ToList(),
                Page = page,
                PageCount = this.db.Count<Link>(x => x.Course.OwnerId == userId) / 10 + 1,
                SortBy = sortBy
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var link = this.db.Find<Link>(x => x.Id == id, "Course");
            if (link == null)
            {
                return HttpNotFound();
            }
            if (link.Course.OwnerId != this.auth.GetCurrentUserId())
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new DetailsViewModel
            {
                CourseId = link.Course.Id,
                Id = link.Id,
                Title = link.Title,
                Url = link.Url,
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var userId = this.auth.GetCurrentUserId();
            var viewModel = new CreateViewModel
            {
                Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title, Url, Course")]CreateViewModel viewModel)
        {
            var userId = this.auth.GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                viewModel.Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                return View(viewModel);
            }

            var link = new Link
            {
                Url = viewModel.Url,
                CourseId = viewModel.Course,
                Title = viewModel.Title
            };

            foreach (var user in this.db.Find<Course>(x => x.Id == viewModel.Course).FavouriteOf)
            {
                this.db.Add<Notification>(new Notification
                {
                    Parameter1 = this.auth.GetCurrentUser().Nickname,
                    Parameter2 = link.Url,
                    Type = NotificationType.LinkAdded,
                    RecipientId = user.Id
                });
            }

            this.db.Add<Link>(link);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var link = this.db.Find<Link>(x => x.Id == id, "Course");
            if (link == null)
            {
                return HttpNotFound();
            }
            if (link.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new EditViewModel
            {
                Url = link.Url,
                Course = link.Course.Id,
                Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                Id = link.Id,
                Title = link.Title
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Title, Url, Course")]EditViewModel viewModel)
        {
            var userId = this.auth.GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                viewModel.Courses = this.db.All<Course>().Where(x => x.OwnerId == userId).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                return View(viewModel);
            }
            var link = this.db.Find<Link>(x => x.Id == viewModel.Id, "Course");
            if (link == null)
            {
                return HttpNotFound();
            }
            if (link.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            link.Url = viewModel.Url;
            link.CourseId = viewModel.Course;
            link.Title = viewModel.Title;

            this.db.Update<Link>(link);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            var userId = this.auth.GetCurrentUserId();
            if (id == null)
            {
                return this.redirectToIndex();
            }
            var link = this.db.Find<Link>(x => x.Id == id, "Course");
            if (link == null)
            {
                return HttpNotFound();
            }
            if (link.Course.OwnerId != userId)
            {
                return new HttpUnauthorizedResult();
            }

            this.db.Delete<Link>(link);
            this.db.SaveChanges();

            return this.redirectToIndex();
        }

        private ActionResult redirectToIndex()
        {
            return Redirect("/dashboard/links");
        }
    }
}