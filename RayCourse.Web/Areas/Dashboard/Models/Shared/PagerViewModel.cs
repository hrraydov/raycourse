﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Shared
{
    public class PagerViewModel
    {
        public int Page { get; set; }

        public int PageCount { get; set; }

        public bool SortingEnabled { get; set; }

        public string SortBy { get; set; }

        public string BaseUrl { get; set; }
    }
}