﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Models.Articles
{
    public class EditViewModel
    {
        public int Id { get; set; }

        [Required(
            ErrorMessageResourceName = "TitleRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Title",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Title { get; set; }

        [Required(
            ErrorMessageResourceName = "ContentRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Display(
            Name = "Content",
            ResourceType = typeof(Resources.Fields)
        )]
        public string Content { get; set; }

        [Required(
            ErrorMessageResourceName = "CourseRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Course",
            ResourceType = typeof(Resources.Common)
        )]
        public int Course { get; set; }

        public IEnumerable<SelectListItem> Courses { get; set; }
    }
}