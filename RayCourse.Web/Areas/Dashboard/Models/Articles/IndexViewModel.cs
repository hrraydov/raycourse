﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Articles
{
    public class IndexViewModel : BaseIndexViewModel
    {
        public IEnumerable<Article> Articles { get; set; }

        public class Article
        {
            public int Id { get; set; }

            public string Title { get; set; }

            public string Course { get; set; }

            public int CourseId { get; set; }

            public DateTime CreatedOn { get; set; }
        }
    }
}