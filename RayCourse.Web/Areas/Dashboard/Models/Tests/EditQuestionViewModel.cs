﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Models.Tests
{
    public class EditQuestionViewModel
    {
        public int Id { get; set; }

        public int TestId { get; set; }

        [Required(
            ErrorMessageResourceName = "TitleRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Title",
            ResourceType = typeof(Resources.Fields)
            )]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [Display(
            Name = "AnswerDescription",
            ResourceType = typeof(Resources.Fields)
            )]
        public string AnswerDescription { get; set; }

        [Display(
            Name = "MultipleAnswers",
            ResourceType = typeof(Resources.Fields)
            )]
        public bool MultipleAnswers { get; set; }

        [Display(
            Name = "Answers",
            ResourceType = typeof(Resources.Common)
            )]
        public ICollection<Answer> Answers { get; set; }

        public class Answer
        {
            public string Content { get; set; }

            public bool IsTrue { get; set; }
        }
    }
}