﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Models.Tests
{
    public class CreateViewModel
    {
        [Required(
            ErrorMessageResourceName = "NameRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Name { get; set; }

        [Required(
            ErrorMessageResourceName = "CourseRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Course",
            ResourceType = typeof(Resources.Common)
        )]
        public int Course { get; set; }

        public IEnumerable<SelectListItem> Courses { get; set; }
    }
}