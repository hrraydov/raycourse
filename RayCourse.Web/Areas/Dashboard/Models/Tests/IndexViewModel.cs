﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Tests
{
    public class IndexViewModel : BaseIndexViewModel
    {
        public IEnumerable<Test> Tests { get; set; }

        public class Test
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public int CourseId { get; set; }

            public string Course { get; set; }

            public int QuestionCount { get; set; }
        }
    }
}