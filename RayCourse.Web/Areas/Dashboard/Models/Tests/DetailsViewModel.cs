﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Tests
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Question> Questions { get; set; }

        public class Question
        {
            public int Id { get; set; }

            public string Title { get; set; }

            public string AnswerDescription { get; set; }

            public bool MultipleAnswers { get; set; }

            public ICollection<Answer> Answers { get; set; }
        }

        public class Answer
        {
            public int Id { get; set; }

            public string Content { get; set; }

            public bool IsTrue { get; set; }
        }
    }
}