﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Courses
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string UrlName { get; set; }

        public string Description { get; set; }

        public string Password { get; set; }

        public CourseType Type { get; set; }

        public IEnumerable<Article> Articles { get; set; }

        public IEnumerable<Link> Links { get; set; }

        public IEnumerable<Test> Tests { get; set; }

        public class Article
        {
            public int Id { get; set; }

            public string Title { get; set; }
        }

        public class Link
        {
            public int Id { get; set; }

            public string Title { get; set; }
        }

        public class Test
        {
            public int Id { get; set; }

            public string Name { get; set; }
        }
    }
}