﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Courses
{
    public class IndexViewModel : BaseIndexViewModel
    {
        public IEnumerable<Course> Courses { get; set; }

        public class Course
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string UrlName { get; set; }

            public CourseType Type { get; set; }
        }
    }
}