﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Courses
{
    public class CreateViewModel
    {
        [Required(
            ErrorMessageResourceName = "NameRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Name { get; set; }

        [Required(
            ErrorMessageResourceName = "DescriptionRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Description",
            ResourceType = typeof(Resources.Fields)
            )]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.Fields)
            )]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(
            Name = "Type",
            ResourceType = typeof(Resources.Fields)
            )]
        public CourseType Type { get; set; }
    }
}