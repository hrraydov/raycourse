﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Profile
{
    public class EditViewModel
    {
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Fields))]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(Resources.Fields))]
        public string LastName { get; set; }

        [Display(Name = "Sex", ResourceType = typeof(Resources.Fields))]
        public Sex Sex { get; set; }

        [Display(Name = "Country", ResourceType = typeof(Resources.Fields))]
        public string Country { get; set; }

        [Display(Name = "City", ResourceType = typeof(Resources.Fields))]
        public string City { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "About", ResourceType = typeof(Resources.Fields))]
        public string About { get; set; }

        [Url(
            ErrorMessage=null,
            ErrorMessageResourceName = "UrlValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(Name = "Website", ResourceType = typeof(Resources.Fields))]
        public string WebSite { get; set; }

        [RegularExpression(
            @"^$|^((http|https)(:(\/\/|\\\\))(www\.)?[fF][aA][cC][eE][bB][oO][oO][kK]\.[cC][oO][mM]\/)(.)+$",
            ErrorMessageResourceName = "FacebookValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
        )]
        [Display(Name = "Facebook", ResourceType = typeof(Resources.Fields))]
        public string Facebook { get; set; }

        [RegularExpression(@"^$|^((http|https)(:(\/\/|\\\\))(www\.)?[pP][lL][uU][sS]\.[gG][oO][oO][gG][lL][eE]\.[cC][oO][mM]\/)(.)+$",
            ErrorMessageResourceName = "GoogleValid",
            ErrorMessageResourceType = typeof(Resources.Validation))]
        [Display(Name = "Google", ResourceType = typeof(Resources.Fields))]
        public string Google { get; set; }

        [RegularExpression(@"^$|^((http|https)(:(\/\/|\\\\))(www\.)?[tT][wW][iI][tT][tT][eE][rR]\.[cC][oO][mM]\/)(.)+$",
            ErrorMessageResourceName = "TwitterValid",
            ErrorMessageResourceType = typeof(Resources.Validation))]
        [Display(Name = "Twitter", ResourceType = typeof(Resources.Fields))]
        public string Twitter { get; set; }

        [RegularExpression(@"^$|^((http|https)(:(\/\/|\\\\))(www\.)?[lL][iI][nN][kK][eE][dD][iI][nN]\.[cC][oO][mM]\/)(.)+$",
            ErrorMessageResourceName = "LinkedInValid",
            ErrorMessageResourceType = typeof(Resources.Validation))]
        [Display(Name = "LinkedIn", ResourceType = typeof(Resources.Fields))]
        public string LinkedIn { get; set; }
    }
}