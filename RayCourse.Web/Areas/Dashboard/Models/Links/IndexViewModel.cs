﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models.Links
{
    public class IndexViewModel : BaseIndexViewModel
    {
        public IEnumerable<Link> Links { get; set; }

        public class Link
        {
            public int Id { get; set; }

            public string Title { get; set; }

            public string Url { get; set; }

            public string Course { get; set; }

            public int CourseId { get; set; }
        }
    }
}