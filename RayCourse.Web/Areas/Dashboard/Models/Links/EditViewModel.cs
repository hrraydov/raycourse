﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Dashboard.Models.Links
{
    public class EditViewModel
    {
        public int Id { get; set; }

        [Required(
            ErrorMessageResourceName = "TitleRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Title",
            ResourceType = typeof(Resources.Fields)
            )]
        public string Title { get; set; }

        [Required(
            ErrorMessage = null,
            ErrorMessageResourceName = "UrlRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Url",
            ResourceType = typeof(Resources.Fields)
            )]
        [Url(
            ErrorMessage = null,
            ErrorMessageResourceName = "UrlValid",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        public string Url { get; set; }

        [Required(
            ErrorMessageResourceName = "CourseRequired",
            ErrorMessageResourceType = typeof(Resources.Validation)
            )]
        [Display(
            Name = "Course",
            ResourceType = typeof(Resources.Common)
        )]
        public int Course { get; set; }

        public IEnumerable<SelectListItem> Courses { get; set; }
    }
}