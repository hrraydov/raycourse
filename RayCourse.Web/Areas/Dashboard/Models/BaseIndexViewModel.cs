﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Dashboard.Models
{
    public class BaseIndexViewModel
    {
        public int Page { get; set; }

        public int PageCount { get; set; }

        public string SortBy { get; set; }
    }
}