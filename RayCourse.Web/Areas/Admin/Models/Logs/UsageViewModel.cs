﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Admin.Models.Logs
{
    public class UsageViewModel
    {
        public ICollection<Controller> Controllers { get; set; }

        public class Controller
        {
            public string Area { get; set; }

            public string Name { get; set; }

            public IDictionary<string, int> Actions { get; set; }

            public IDictionary<string, int> Methods { get; set; }
        }
    }
}