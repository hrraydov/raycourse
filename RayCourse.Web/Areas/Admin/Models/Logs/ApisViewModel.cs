﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayCourse.Web.Areas.Admin.Models.Logs
{
    public class ApisViewModel
    {
        public int Page { get; set; }

        public IEnumerable<Log> Logs { get; set; }

        public class Log
        {
            public string Controller { get; set; }

            public string Action { get; set; }

            public string Params { get; set; }

            public string Uri { get; set; }

            public string Method { get; set; }

            public string IP { get; set; }

            public string Email { get; set; }

            public DateTime DateTime { get; set; }
        }
    }
}