﻿using RayCourse.Data;
using RayCourse.Models;
using RayCourse.Web.Areas.Admin.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayCourse.Web.Areas.Admin.Controllers
{
    [RouteArea("Admin")]
    [Authorize(Roles = "Admin")]
    public class LogsController : Controller
    {
        private const int pageSize = 20;

        private IAppContext db;

        public LogsController()
        {
            this.db = new AppContext();
        }

        public LogsController(IAppContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Index(int page = 1, string email = "")
        {
            var logs = this.db.All<Log>().Where(x => !x.Request.IsApiAction && !x.Request.IsChildAction);
            if (!String.IsNullOrWhiteSpace(email))
            {
                logs = logs.Where(x => x.Email == email);
            }
            logs = logs.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize);
            return View(new IndexViewModel
            {
                Logs = logs.Select(x => new IndexViewModel.Log
                {
                    Action = x.Request.Action,
                    Area = x.Request.Area,
                    Controller = x.Request.Controller,
                    DateTime = x.DateTime,
                    Email = x.Email,
                    IP = x.IP,
                    Method = x.Request.Method,
                    Params = x.Request.Params,
                    Uri = x.Request.Uri
                }).ToList(),
                Page = page
            });
        }

        [HttpGet]
        public ActionResult Apis(int page = 1)
        {
            var logs = this.db.All<Log>().Where(x => x.Request.IsApiAction).OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize);
            return View(new ApisViewModel
            {
                Logs = logs.Select(x => new ApisViewModel.Log
                {
                    Action = x.Request.Action,
                    Controller = x.Request.Controller,
                    DateTime = x.DateTime,
                    Email = x.Email,
                    IP = x.IP,
                    Method = x.Request.Method,
                    Params = x.Request.Params,
                    Uri = x.Request.Uri
                }),
                Page = page
            });
        }

        [HttpGet]
        public ActionResult Usage()
        {
            var logs = this.db.All<Log>().Where(x => !x.Request.IsApiAction && !x.Request.IsChildAction).ToList();
            var viewModel = new UsageViewModel
            {
                Controllers = new HashSet<UsageViewModel.Controller>()
            };
            foreach (var log in logs)
            {
                var controller = log.Request.Controller;
                var action = log.Request.Action;
                var area = log.Request.Area;
                var method = log.Request.Method;
                var ctrl = viewModel.Controllers.FirstOrDefault(x => x.Area == area && x.Name == controller);
                if (ctrl == null)
                {
                    viewModel.Controllers.Add(new UsageViewModel.Controller
                    {
                        Name = controller,
                        Area = area,
                        Actions = new Dictionary<string, int>(){
                            {action,1}
                        },
                        Methods = new Dictionary<string, int>(){
                            {method,1}
                        }
                    });
                }
                else
                {
                    if (ctrl.Actions.ContainsKey(action))
                    {
                        ctrl.Actions[action]++;
                    }
                    else
                    {
                        ctrl.Actions.Add(action, 1);
                    }
                    if (ctrl.Methods.ContainsKey(method))
                    {
                        ctrl.Methods[method]++;
                    }
                    else
                    {
                        ctrl.Methods.Add(method, 1);
                    }
                }
            }
            viewModel.Controllers = viewModel.Controllers.OrderByDescending(x => x.Actions.Values.Sum()).ToList();
            return View(viewModel);
        }

        [HttpGet]
        [ActionName("Most-Active")]
        public ActionResult MostActive()
        {
            Dictionary<string, int> users = this.db.All<Log>()
                .Where(x => !x.Request.IsApiAction && !x.Request.IsChildAction)
                .GroupBy(x => x.Email)
                .OrderByDescending(x => x.Count())
                .Take(20)
                .ToDictionary(x => x.Key, x => x.Count());
            return View(users);
        }
    }
}