﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public class Test
    {
        private ICollection<Question> questions;

        public Test()
        {
            this.questions = new HashSet<Question>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<Question> Questions
        {
            get { return this.questions; }
            set { this.questions = value; }
        }

        [Required]
        public int CourseId { get; set; }
        public virtual Course Course { get; set; }
    }
}
