﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    [ComplexType]
    public class Request
    {
        public string Uri { get; set; }

        public string Method { get; set; }

        public string Area { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Params { get; set; }

        public bool IsApiAction { get; set; }

        public bool IsChildAction { get; set; }
    }
}
