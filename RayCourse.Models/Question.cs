﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public class Question
    {
        private ICollection<Answer> answers;

        public Question()
        {
            this.answers = new HashSet<Answer>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public virtual ICollection<Answer> Answers
        {
            get { return this.answers; }
            set { this.answers = value; }
        }

        [Required]
        public string TrueAnswers { get; set; }

        [Required]
        public bool MultipleAnswers { get; set; }

        public string AnswerDescription { get; set; }

        [Required]
        public int TestId { get; set; }
        public virtual Test Test { get; set; }

    }
}
