﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateTime { get; set; }

        public string Email { get; set; }

        public string IP { get; set; }

        public Request Request { get; set; }
    }
}
