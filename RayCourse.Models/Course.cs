﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public class Course
    {
        private ICollection<User> favouriteOf;
        private ICollection<Article> articles;
        private ICollection<Link> links;
        private ICollection<Test> tests;

        public Course()
        {
            this.favouriteOf = new HashSet<User>();
            this.articles = new HashSet<Article>();
            this.links = new HashSet<Link>();
            this.tests = new HashSet<Test>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }
        
        [Required]
        public string Description { get; set; }

        [Required]
        public CourseType Type { get; set; }

        public string Password { get; set; }

        [Required]
        public string OwnerId { get; set; }

        public virtual User Owner { get; set; }

        public virtual ICollection<User> FavouriteOf
        {
            get { return this.favouriteOf; }
            set { this.favouriteOf = value; }
        }

        public virtual ICollection<Article> Articles
        {
            get { return this.articles; }
            set { this.articles = value; }
        }

        public virtual ICollection<Link> Links
        {
            get { return this.links; }
            set { this.links = value; }
        }

        public virtual ICollection<Test> Tests
        {
            get { return this.tests; }
            set { this.tests = value; }
        }
    }
}
