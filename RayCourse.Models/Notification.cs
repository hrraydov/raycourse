﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public class Notification
    {
        [Key]
        public int Id { get; set; }

        public NotificationType Type { get; set; }

        public string Parameter1 { get; set; }

        public string Parameter2 { get; set; }

        public bool Read { get; set; }

        [Required]
        public string RecipientId { get; set; }
        public virtual User Recipient { get; set; }
    }
}
