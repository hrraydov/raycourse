﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public enum NotificationType
    {
        AddedToFavourites,
        ArticleCommented,
        ArticleAdded,
        LinkAdded,
        TestAdded,
    }
}
