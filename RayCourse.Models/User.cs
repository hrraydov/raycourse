﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public class User : IdentityUser
    {
        private ICollection<Course> ownedCourses;
        private ICollection<Course> favouriteCourses;
        private ICollection<Notification> notifications;

        public User()
        {
            this.ownedCourses = new HashSet<Course>();
            this.favouriteCourses = new HashSet<Course>();
            this.notifications = new HashSet<Notification>();
        }

        [Required]
        public string Nickname { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Sex Sex { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string About { get; set; }

        public string WebSite { get; set; }

        public string Skype { get; set; }

        public string Facebook { get; set; }

        public string Google { get; set; }

        public string Twitter { get; set; }

        public string LinkedIn { get; set; }

        public virtual ICollection<Course> OwnedCourses
        {
            get { return this.ownedCourses; }
            set { this.ownedCourses = value; }
        }

        public virtual ICollection<Course> FavouriteCourses
        {
            get { return this.favouriteCourses; }
            set { this.favouriteCourses = value; }
        }

        public virtual ICollection<Notification> Notifications
        {
            get { return this.notifications; }
            set { this.notifications = value; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
