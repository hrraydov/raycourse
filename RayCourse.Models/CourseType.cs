﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public enum CourseType
    {
        [Display(
            Name = "Public",
            ResourceType = typeof(Resources.Common)
            )]
        Public = 0,
        [Display(
            Name = "Private",
            ResourceType = typeof(Resources.Common)
            )]
        Private = 1,
        [Display(
            Name = "Hidden",
            ResourceType = typeof(Resources.Common)
            )]
        Hidden = 2
    }

    public static class CourseTypeExtensions
    {
        public static string ToFriendlyString(this CourseType type)
        {
            switch (type)
            {
                case CourseType.Hidden:
                    return RayCourse.Resources.Common.Hidden;
                case CourseType.Private:
                    return RayCourse.Resources.Common.Private;
                default:
                    return RayCourse.Resources.Common.Public;
            }
        }
    }
}
