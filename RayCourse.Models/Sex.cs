﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Models
{
    public enum Sex
    {
        [Display(
            Name = "Male",
            ResourceType = typeof(Resources.Common)
            )]
        Male,
        [Display(
            Name = "Female",
            ResourceType = typeof(Resources.Common)
            )]
        Female
    }

    public static class SexExtensions
    {
        public static string ToFriendlyString(this Sex sex)
        {
            switch (sex)
            {
                case Sex.Male:
                    return RayCourse.Resources.Common.Male;
                default:
                    return RayCourse.Resources.Common.Female;
            }
        }
    }
}
