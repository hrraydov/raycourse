﻿using Microsoft.AspNet.Identity.EntityFramework;
using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Data
{
    public class AppContext : IdentityDbContext<User>, IAppContext
    {
        public AppContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Database.Log = s => Debug.WriteLine(s);
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Notification> Notifications { get; set; }

        public static AppContext Create()
        {
            return new AppContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(x => x.OwnedCourses)
                .WithRequired(x => x.Owner)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<User>()
                .HasMany(x => x.FavouriteCourses)
                .WithMany(x => x.FavouriteOf)
                .Map(m =>
                {
                    m.MapLeftKey("UserId");
                    m.MapRightKey("CourseId");
                    m.ToTable("UserCourses");
                });
            base.OnModelCreating(modelBuilder);
        }

        public IQueryable<T> All<T>() where T : class
        {
            return this.Set<T>();
        }

        public T Find<T>(Expression<Func<T, bool>> filter, string includeProperties = null) where T : class
        {
            IQueryable<T> query = this.Set<T>();

            if (!String.IsNullOrEmpty(includeProperties))
            {
                foreach (var include in includeProperties.Split(','))
                {
                    query = query.Include(include);
                }
            }

            return query.FirstOrDefault(filter);
        }

        public void Add<T>(T item) where T : class
        {
            this.Set<T>().Add(item);
        }

        public void Update<T>(T item) where T : class
        {
            this.Entry(item).State = EntityState.Modified;
        }

        public void Delete<T>(T item) where T : class
        {
            if (typeof(T) == typeof(User))
            {
                var user = item as User;
                var ownedCourses = user.OwnedCourses.ToList();
                foreach (var course in ownedCourses)
                {
                    this.Delete<Course>(course);
                }
                this.Set<User>().Remove(user);
            }
            else
            {
                this.Set<T>().Remove(item);
            }
        }

        public void DeleteBy<T>(Expression<Func<T, bool>> filter) where T : class
        {
            var item = this.Find<T>(filter);
            this.Delete<T>(item);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }


        public int Count<T>(Expression<Func<T, bool>> filter = null) where T : class
        {
            IQueryable<T> query = this.Set<T>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.Count();
        }
    }
}
