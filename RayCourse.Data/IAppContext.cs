﻿using RayCourse.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RayCourse.Data
{
    public interface IAppContext
    {
        IQueryable<T> All<T>() where T : class;

        T Find<T>(Expression<Func<T, bool>> filter, string includeProperties = null) where T : class;

        void Add<T>(T item) where T : class;

        void Update<T>(T item) where T : class;

        void Delete<T>(T item) where T : class;

        void DeleteBy<T>(Expression<Func<T, bool>> filter) where T : class;

        int Count<T>(Expression<Func<T, bool>> filter = null) where T : class;

        int SaveChanges();
    }
}
